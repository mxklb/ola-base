FROM ubuntu:20.04

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \ 
    libcppunit-dev libcppunit-dev uuid-dev pkg-config libncurses5-dev libtool \
    autoconf automake g++ libmicrohttpd-dev libmicrohttpd12 protobuf-compiler \
    libprotobuf-lite17 python3-protobuf libprotobuf-dev libprotoc-dev zlib1g-dev \
    bison flex make libftdi-dev libftdi1 libusb-1.0-0-dev liblo-dev \
    libavahi-client-dev git avahi-daemon supervisor